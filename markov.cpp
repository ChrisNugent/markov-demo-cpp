#include <map>
#include <iostream>
#include <string>
#include <stdlib.h>     // for rand and srand
#include <ctime>
using namespace std;

void addToMap(const string& input, map<char, map<char, int> >& wordMap);
string makeWord(const map<char, map<char, int> >& wordMap);

int main() {
    srand(time(NULL));
    map<char, map<char, int> > wordMap;

    /* Input */
    string input;
    cout << "Please provide input for mashing, or \"q\" to finish." << endl;
    getline(cin, input);
    while(input != "q") {
        addToMap(input, wordMap);
        getline(cin, input);
    }

    /* Output */
    int count;
    cout << "How many results to generate? "; 
    cin >> count;
    while(count-- > 0) {
        cout << makeWord(wordMap) << endl;
    }
}

void addToMap(const string& input, map<char, map<char, int> >& wordMap) {
    char source, dest;
    for(int i = 0; i < input.length() - 1; i++) {
        source = input.at(i);
        dest = input.at(i + 1);
        // When we use the substring operator, empty values are automatically initialized
        wordMap[source][dest]++;
    }
    // Insert start and end terminators
    wordMap[dest]['\0']++;
    wordMap['\0'][input.at(0)]++;
}

string makeWord(const map<char, map<char, int> >& wordMap) {
    char current_state = '\0';
    string result = "";    
    do {
        // Add up each letter count
        int sumOfCounts = 0;
        for(map<char, int>::const_iterator it = wordMap.at(current_state).begin();
            it != wordMap.at(current_state).end(); it++) {
            sumOfCounts += it->second;
        }
        // Create a random within sumOfCounts, and count down
        // from it.
        int r = rand() % sumOfCounts;
        for(map<char, int>::const_iterator it = wordMap.at(current_state).begin();
            it != wordMap.at(current_state).end(); it++) {
            r -= it->second;
            if(r < 0) {
                // Update state and append to result
                current_state = it->first;
                result += current_state;
                break;
            }
        }
    // break on a null terminator
    } while (current_state != '\0');
    return result;
}